# A small launcher app

A simple launcher app in rust.


# Modes

This app has 2 modes.

## App launcher

This mode is for selecting and launching app. App creates a db in `$HOME/.config/launcher` folder containing a list of apps installed on the system on first launch.
Passing `--reset-app-db` cleans and builds the database again.

## Menu

This mode is like `dmenu`. It allows applications to show options in UI and get selection. `-p` specifies prompt and specify the options as new line separated list
of strings in stdin.


# UI

The app uses slint as UI framework. It shows a search box and a list of search options which a user can select. Pressing `Tab` on a selection triggers alternate options.

