use crate::{OptionEntry, SelectionStyle};

use ouroboros::self_referencing;
use slint::{Image, SharedString};


#[derive(Debug, PartialEq, Eq)]
pub struct OrderedOption {
    id: u64,
    name: String,
}

impl OrderedOption {
    pub fn to_option_entry(&self) -> OptionEntry {
        OptionEntry { data: self.name.clone().into(), icon: Image::default(), index: self.id as i32 }
    }

    pub fn invoke(&self, _style: SelectionStyle) {
        println!("{}", self.name);
        std::process::exit(0);
    }
}


#[self_referencing]
pub struct Searcher {
    options: Vec<String>,

    #[borrows(options)]
    #[covariant]
    keys: Vec<&'this str>,
}

impl Searcher {
    pub fn initialize() -> Self {
        let options = {
            let mut vec = Vec::new();

            for line in std::io::stdin().lines() {
                match line {
                    Ok(l) => {
                        vec.push(l);
                    },
                    Err(e) => {
                        eprintln!("Error reading from stdin. {e}");
                    },
                }
            }

            vec
        };

        SearcherBuilder {
            options,
            keys_builder: |opt: &Vec<String>| {
                let mut vec = Vec::new();

                for k in opt {
                    vec.push(k.as_str());
                }

                vec
            },
        }.build()
    }

    pub fn search(&self, text: SharedString) -> Vec<OrderedOption> {

        let options = rust_fuzzy_search::fuzzy_search_sorted(&text, self.borrow_keys());
        let mut vec = Vec::new();

        let mut count = 0;
        for (index, (opt, score)) in options.into_iter().enumerate() {
            if (count <= 2 && score >= 0.2) || score >= 0.5 {
                vec.push(OrderedOption { id: index as u64, name: opt.to_string() });
                count += 1;
            }
        }
        vec
    }

    pub fn all_options(&self) -> Vec<OrderedOption> {
        let mut vec = Vec::new();

        for (index, opt) in self.borrow_options().iter().enumerate() {
            vec.push(OrderedOption { id: index as u64, name: opt.to_string() });
        }

        vec
    }
}

