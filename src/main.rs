use std::{rc::Rc, sync::Arc};

use slint::{ModelRc, VecModel, Weak};
use state::PossibleOptions;

use crate::menu::OrderedOption;

slint::include_modules!();

pub mod cmd;
mod state;

mod apps;
mod menu;


fn main() -> Result<(), slint::PlatformError> {

    cmd::parse_command_line_options();

    let search_box = SearchWindow::new()?;

    if cmd::get_command_line_options().dmenu {
        setup_menu_search_mode(&search_box);
    } else {
        setup_app_search_mode(&search_box);
    }

    {
        search_box.on_exit_app(move || {
            std::process::exit(0);
        });
    }

    search_box.run()
}

fn setup_app_search_mode(search_box: &SearchWindow) {
    let search_options = Arc::new(PossibleOptions::new());

    search_box.set_show_icon(true);
    search_box.set_prompt("🔎︎  Search".into());

    {
        let mut searcher = apps::Searcher::new();

        let search_options_copy = search_options.clone();
        let search_box_weak_copy = search_box.as_weak();

        search_box.on_searched(move |s| {
            let search_box_weak_copy = search_box_weak_copy.clone();
            let search_options_copy = search_options_copy.clone();

            searcher.search(s, move |options| {
                let mut vec = Vec::new();

                for opt in &options {
                    vec.push(opt.to_option_entry());
                }

                search_options_copy.set(options);

                let options_model = Rc::new(VecModel::from(vec));
                search_box_weak_copy.upgrade().unwrap().set_options(ModelRc::from(options_model));
            }).unwrap();
        });
    }


    {
        let search_options_copy = search_options.clone();
        search_box.on_selected(move |option, style| {
            let options = search_options_copy.get();

            if option >= 0 && (option as usize) < options.len() {
                if let Some(option) = options.get(option as usize) {
                    option.invoke(style);
                }
            }
        });
    }
}

fn setup_menu_search_mode(search_box: &SearchWindow) {
    let search_options = Arc::new(PossibleOptions::new());

    search_box.set_show_icon(false);

    let prompt = match cmd::get_command_line_options().prompt.as_ref() {
        Some(p) => if p.is_empty() { "🔎︎  ".to_string() } else { p.to_string() },
        None => "🔎︎  ".to_string(),
    };
    search_box.set_prompt(prompt.into());

    {
        let searcher = menu::Searcher::initialize();

        let search_options_copy = search_options.clone();
        let search_box_weak_copy = search_box.as_weak();

        fn set_options_to_show(opts: Vec<OrderedOption>, search_box: Weak<SearchWindow>, options: Arc<PossibleOptions<OrderedOption>>) {
            let mut vec = Vec::new();

            for opt in &opts {
                vec.push(opt.to_option_entry());
            }

            options.set(opts);

            let options_model = Rc::new(VecModel::from(vec));
            search_box.upgrade().unwrap().set_options(ModelRc::from(options_model));
        }

        set_options_to_show(searcher.all_options(), search_box_weak_copy.clone(), search_options.clone());

        search_box.on_searched(move |s| {
            let search_box_weak_copy = search_box_weak_copy.clone();
            let search_options_copy = search_options_copy.clone();

            let opts = if s.trim().is_empty() {
                searcher.all_options()
            } else {
                searcher.search(s)
            };

            let mut vec = Vec::new();

            for opt in &opts {
                vec.push(opt.to_option_entry());
            }

            search_options_copy.set(opts);

            let options_model = Rc::new(VecModel::from(vec));
            search_box_weak_copy.upgrade().unwrap().set_options(ModelRc::from(options_model));
        });
    }


    {
        let search_options_copy = search_options.clone();
        search_box.on_selected(move |option, style| {
            let options = search_options_copy.get();

            if option >= 0 && (option as usize) < options.len() {
                if let Some(option) = options.get(option as usize) {
                    option.invoke(style);
                }
            }
        });
    }
}
