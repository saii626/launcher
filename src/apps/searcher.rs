use crate::{OptionEntry, SelectionStyle};

use std::process::Command;
use std::thread;
use std::sync::{atomic::Ordering, Arc};

use atomic_enum::atomic_enum;
use slint::Image;
use xdotool::command::options;
use xdotool::{keyboard, option_vec, window, OptionVec};

use super::EntryData;

#[atomic_enum]
#[derive(PartialEq, Eq)]
enum OngoingSearchState {
    Searching,
    Canceled,
    Completed,
}

#[derive(Debug, Clone)]
pub struct OngoingSearchStateRef(Arc<AtomicOngoingSearchState>);

impl OngoingSearchStateRef {
    pub fn new() -> Self {
        Self(Arc::new(AtomicOngoingSearchState::new(OngoingSearchState::Searching)))
    }

    pub fn is_searching(&self) -> bool {
        self.0.load(Ordering::Acquire) == OngoingSearchState::Searching
    }

    pub fn cancel_if_searching(&self) -> bool {
        self.0.compare_exchange(OngoingSearchState::Searching, OngoingSearchState::Canceled, Ordering::Acquire, Ordering::Relaxed).is_ok()
    }

    pub fn complete_if_searching(&self) -> bool {
        self.0.compare_exchange(OngoingSearchState::Searching, OngoingSearchState::Completed, Ordering::SeqCst, Ordering::Acquire).is_ok()
    }
}

#[derive(Debug)]
pub struct OngoingSearch {
    _handle: thread::JoinHandle<()>,
    state: OngoingSearchStateRef,
}

impl OngoingSearch {
    pub fn new(handle: thread::JoinHandle<()>, state: OngoingSearchStateRef) -> Self {
        Self { _handle: handle, state }
    }

    pub fn state(&self) -> OngoingSearchStateRef {
        self.state.clone()
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct SearchOption {
    id: u64,
    name: String,
    data: EntryData,
}

impl SearchOption {
    pub fn to_option_entry(&self) -> OptionEntry {
        let icon_path = match &self.data {
            EntryData::Application { icon, .. } => { icon.clone() },
            EntryData::Script { icon, .. } => icon.clone(),
            EntryData::Command { icon, .. } => icon.clone(),
        };

        let icon = icon_path.map(|icon| Image::load_from_path(&icon).unwrap_or_else(|_| panic!("Unable to load image {icon:?}")));

        OptionEntry { data: self.name.clone().into(), index: self.id as i32, icon: icon.unwrap_or(Image::default()) }
    }

    pub fn invoke(&self, style: SelectionStyle) {
        println!("Invoking option {:?}", self);

        match &self.data {
            EntryData::Application { path, args, .. } => {
                if let Err(e) = Command::new(path)
                    .args(args)
                    .spawn() {
                    eprintln!("Unable to launch app {path:?} {args:?}. {e}");
                } else {
                    std::process::exit(0);
                }
            },
            EntryData::Script { path, .. } => {
                match style {
                    SelectionStyle::Normal => {
                        if let Err(e) = Command::new("st")
                                .arg("-e")
                                .arg(path)
                                .spawn() {
                            eprintln!("Unable to run script {path:?}. {e}");
                        } else {
                            std::process::exit(0);
                        }
                    },
                    SelectionStyle::Alternate => {
                        match Command::new("st").spawn() {
                            Ok(c) =>{
                                let output = window::search("st", option_vec![
                                                            options::SearchOption::Name,
                                                            options::SearchOption::Pid(c.id()),
                                                            options::SearchOption::All,
                                                            options::SearchOption::Sync]);

                                if output.status.success() {
                                    let window_id = {
                                        let s = String::from_utf8(output.stdout).unwrap();
                                        s.trim().to_string()
                                    };
                                    let path_str = format!("{path:?}");
                                    for ch in path_str.chars() {
                                        if let Some(r) = x11_keysymdef::lookup_by_codepoint(ch) {
                                            if !r.names.is_empty() {
                                                keyboard::send_key(r.names[0], option_vec![
                                                                   options::KeyboardOption::Window(window_id.clone()),
                                                                   options::KeyboardOption::Delay(0)]);
                                            }
                                        }
                                    }

                                    std::process::exit(0);
                                } else {
                                    eprintln!("Unable to find terminal window {}. ", output.status);
                                    std::process::exit(1);
                                }
                            },
                            Err(e) => {
                                eprintln!("Unable to open terminal `st`. {e}");
                            },
                        }
                    },
                }
            },
            EntryData::Command { path, .. } => {
                if let Err(e) = Command::new(path)
                        .spawn() {
                    eprintln!("Unable to run command {path:?}. {e}");
                } else {
                    std::process::exit(0);
                }
            },
        }
    }
}


#[derive(Debug)]
pub struct OptionsAccumulator {
    current_index: u64,
    options: Vec<SearchOption>,
}

impl OptionsAccumulator {
    pub fn new() -> Self {
        Self {
            current_index: 0,
            options: Vec::new(),
        }
    }

    pub fn add_option(&mut self, name: String, entry: EntryData) {
        let id = self.current_index;
        self.current_index += 1;

        self.options.push(SearchOption { id, name, data: entry })
    }

    pub fn options(self) -> Vec<SearchOption> {
        self.options
    }
}

