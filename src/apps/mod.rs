use std::{path::PathBuf, sync::Arc, thread};

use serde::{Deserialize, Serialize};
use slint::{EventLoopError, SharedString};

use self::db::DatabaseHandle;
use self::searcher::{OngoingSearch, OngoingSearchStateRef, OptionsAccumulator, SearchOption};


mod searcher;
mod db;
mod entry_table;

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub enum EntryData {
    Application { path: PathBuf, args: Vec<String>, icon: Option<PathBuf> },
    Script { path: PathBuf, icon: Option<PathBuf> },
    Command { path: PathBuf, icon: Option<PathBuf> }
}

#[derive(Debug)]
pub struct Searcher {
    database: Arc<DatabaseHandle>,
    ongoing: Option<OngoingSearch>,
}

impl Searcher {
    pub fn new() -> Self {
        Self {
            database: DatabaseHandle::initialize_async(),
            ongoing: None,
        }
    }

    pub fn search<F>(&mut self, text: SharedString, callback: F) -> Result<(), EventLoopError>
        where F: FnOnce(Vec<SearchOption>) + Send + 'static {

        if let Some(ongoing) = &self.ongoing {
            ongoing.state().cancel_if_searching();
        }

        let state_ref = OngoingSearchStateRef::new();

        let state_ref_copy = state_ref.clone();
        let database_handle_copy = self.database.clone();

        let handle = thread::spawn(move || {

            let mut accumulator = OptionsAccumulator::new();
            database_handle_copy.search(&text, state_ref_copy.clone(), &mut accumulator);

            if state_ref_copy.complete_if_searching() {
                slint::invoke_from_event_loop(move || {
                    callback(accumulator.options());
                }).unwrap();
            } else {
                println!("Search was canceled.");
            }
        });

        self.ongoing = Some(OngoingSearch::new(handle, state_ref));

        Ok(())
    }
}
