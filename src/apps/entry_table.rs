use std::collections::hash_map::Entry;
use std::{collections::HashMap, env};
use std::path::{Path, PathBuf};
use std::io::Read;
use std::fs::{create_dir_all, File};

use configparser::ini::Ini;
use is_executable::IsExecutable;
use mime::Mime;
use serde::{Deserialize, Serialize};
use xdg_mime::SharedMimeInfo;

use crate::cmd;

use super::EntryData;


#[derive(Debug, Default, Serialize, Deserialize)]
pub struct SerializableDb (pub HashMap<String, EntryData>);

impl SerializableDb {

    fn read_file(db_file: PathBuf) -> SerializableDb {
        let db_file_handle = match File::open(&db_file) {
            Ok(f) => f,
            Err(e) => {
                panic!("Unable to open db {:?}. {e}", db_file)
            },
        };

        let db: SerializableDb = match serde_json::from_reader(db_file_handle) {
            Ok(d) => d,
            Err(e) => {
                panic!("Unable to parse db {:?}. {e}", db_file)
            }
        };

        db
    }

    pub fn read_system_db() -> SerializableDb {
        let db_file = match dirs::config_dir() {
            Some(mut c) => {
                c.push("quicky");
                create_dir_all(c.as_path()).unwrap();

                c.push("db");

                if cmd::get_command_line_options().reset_app_db && c.exists() {
                    if let Err(e) = std::fs::remove_file(&c) {
                        eprintln!("Unable to delete app db {c:?}. {e}");
                    }
                }

                if !c.exists() {
                    create_new_system_db(c.as_path());
                }

                c
            },
            None => {
                eprintln!("Unable to get config directory");
                std::process::exit(1);
            },
        };

        Self::read_file(db_file)
    }

    fn add_entry(&mut self, name: String, entry: EntryData) {
        match self.0.entry(name.clone()) {
            Entry::Occupied(o) => println!("Entry for {name} already exists {o:?}"),
            Entry::Vacant(v) => { v.insert(entry); },
        }
    }
}

const APPLICATIONS_FOLDER: [&str; 2] = [
    "/usr/share/applications/",
    "/usr/local/share/applications/"
];

const ICONS_FOLDER: [&str; 30] = [
    "/usr/share/icons/hicolor/scalable/apps",
    "/usr/share/icons/hicolor/symbolic/apps",
    "/usr/share/icons/Adwaita/scalable/mimetypes",
    "/usr/share/icons/Adwaita/scalable/places",
    "/usr/share/icons/Adwaita/scalable/devices",
    "/usr/share/icons/Adwaita/scalable/status",
    "/usr/share/icons/Adwaita/symbolic/actions",
    "/usr/share/icons/Adwaita/symbolic/apps",
    "/usr/share/icons/Adwaita/symbolic/categories",
    "/usr/share/icons/Adwaita/symbolic/devices",
    "/usr/share/icons/Adwaita/symbolic/emblems",
    "/usr/share/icons/Adwaita/symbolic/emotes",
    "/usr/share/icons/Adwaita/symbolic/legacy",
    "/usr/share/icons/Adwaita/symbolic/mimetypes",
    "/usr/share/icons/Adwaita/symbolic/places",
    "/usr/share/icons/Adwaita/symbolic/status",
    "/usr/share/icons/Adwaita/symbolic/ui",
    "/usr/share/icons/hicolor/32x32/apps",
    "/usr/share/icons/hicolor/16x16/apps",
    "/usr/share/icons/hicolor/48x48/apps",
    "/usr/share/icons/hicolor/96x96/apps",
    "/usr/share/icons/hicolor/128x128/apps",
    "/usr/share/icons/hicolor/256x256/apps",
    "/usr/share/icons/hicolor/apps",
    "/usr/share/icons/Adwaita/16x16/devices",
    "/usr/share/icons/Adwaita/16x16/emblems",
    "/usr/share/icons/Adwaita/16x16/mimetypes",
    "/usr/share/icons/Adwaita/16x16/places",
    "/usr/share/icons/Adwaita/symbolic/legacy",
    "/usr/share/pixmaps",
];

fn create_new_system_db(path: &Path) {
    let mut new_db = SerializableDb(HashMap::new());
    let mime_info = SharedMimeInfo::new();

    let (apps_in_path, scripts_in_path) = {
        let mut apps = HashMap::new();
        let mut scripts = HashMap::new();


        match env::var("PATH") {
            Ok(p) => {
                for path in env::split_paths(&p) {

                    for entry in path.read_dir().unwrap_or_else(|e| panic!("Unable to read directory {:?}. {e}", path)).flatten() {
                        let entry_path = entry.path();

                        if entry_path.is_file() {
                            match File::open(&entry_path) {
                                Ok(mut f) => {
                                    let mut buf = [0_u8; 64];
                                    if let Err(e) = f.read(&mut buf) {
                                        eprintln!("{entry_path:?}: Unable to read file. {e}");
                                        continue;
                                    }

                                    let mime = {
                                        match mime_info.get_mime_type_for_data(&buf) {
                                            Some((m, c)) => {
                                                if c < 40 {
                                                    eprintln!("{entry_path:?}: Not too confident about the type of file. Asuming {m} with {c} confidence");
                                                }
                                                m
                                            },
                                            None => {
                                                eprintln!("{entry_path:?}: Unable to figure out the mime type.");
                                                mime::APPLICATION_OCTET_STREAM
                                            },
                                        }

                                    };

                                    if buf[0..4] == [0x7f, b'E', b'L', b'F'] && entry_path.is_executable(){
                                        apps.insert(entry_path.file_stem().unwrap().to_str().unwrap().to_string(), (entry_path, mime));
                                    } else if buf[0..2] == [b'#', b'!'] && entry_path.is_executable() {
                                        scripts.insert(entry_path.file_stem().unwrap().to_str().unwrap().to_string(), (entry_path, mime));
                                    }
                                },
                                Err(e) => eprintln!("{entry_path:?}: Unable to open file. {e}"),
                            }
                        }
                    }
                }
            },
            Err(e) => {
                eprintln!("Unbale to read `PATH` environment variable. {e}");
            },
        }

        (apps, scripts)
    };

    let icons_in_path = {
        let mut map = HashMap::new();

        for folder in ICONS_FOLDER {
            let icon_folder = PathBuf::from(folder);
            if !icon_folder.exists() || !icon_folder.is_dir() { continue; }

            for entry in icon_folder.read_dir().unwrap_or_else(|e| panic!("Unable to read directory {:?}. {e}", path)).flatten() {
                let entry_path = entry.path();
                let icon_name = entry_path.file_stem().unwrap().to_str().unwrap().to_string();

                if !map.contains_key(&icon_name) && entry_path.exists() && entry_path.is_file() &&
                    entry_path.extension().map(|ext| ext == "svg" || ext == "png").unwrap_or(false) {

                    map.insert(icon_name, entry_path);
                }
            }
        }

        map
    };

    let app_folders = {
        let mut vec = Vec::new();

        let user_app_folder = dirs::data_local_dir().map(|mut d| {
            d.push("applications");
            d
        });
        if let Some(user_app_folder) = user_app_folder {
            if user_app_folder.exists() && user_app_folder.is_dir() {
                vec.push(user_app_folder);
            }
        }

        for sys_app_folder in APPLICATIONS_FOLDER {
            let path = PathBuf::from(sys_app_folder);
            if path.exists() && path.is_dir() {
                vec.push(path);
            }
        }

        vec
    };

    for app_folder in app_folders {
        for entry in app_folder.read_dir().unwrap_or_else(|e| panic!("Unable to read directory {:?}. {e}", path)).flatten() {
            let entry_path = entry.path();
            let mut ini = Ini::new();

            if entry_path.exists() && entry_path.is_file() && entry_path.extension().map(|ext| ext == "desktop").unwrap_or(false) {
                if let Some(entry_data) = parse_desktop_file(entry_path.clone(), &mut ini, &apps_in_path, &scripts_in_path, &icons_in_path) {
                    new_db.add_entry(entry.path().file_stem().unwrap().to_str().unwrap().to_string(), entry_data);
                }
            }
        }
    }

    for (k, v) in scripts_in_path {
        if !new_db.0.contains_key(&k) {

            let icon = {
                let mime_icons = mime_info.lookup_icon_names(&v.1);

                let mut icon = None;

                for mime_ico in mime_icons {
                    if let Some(icon_path) = icons_in_path.get(&mime_ico) {
                        icon = Some(icon_path.clone());
                        break;
                    }
                }

                if icon.is_none() {
                    if let Some(gen_icon) = mime_info.lookup_generic_icon_name(&v.1) {
                        if let Some(icon_path) = icons_in_path.get(&gen_icon) {
                            icon = Some(icon_path.clone());
                        }
                    }
                }

                icon
            };

            if icon.is_none() {
                eprintln!("{:?}: No icon found.", v.0);
            }
            new_db.add_entry(k, EntryData::Script { path: v.0, icon })
        }
    }

    for (k, v) in apps_in_path {
        if !new_db.0.contains_key(&k) {
            let icon = {
                let mime_icons = mime_info.lookup_icon_names(&v.1);

                let mut icon = None;

                for mime_ico in mime_icons {
                    if let Some(icon_path) = icons_in_path.get(&mime_ico) {
                        icon = Some(icon_path.clone());
                        break;
                    }
                }

                if icon.is_none() {
                    if let Some(gen_icon) = mime_info.lookup_generic_icon_name(&v.1) {
                        if let Some(icon_path) = icons_in_path.get(&gen_icon) {
                            icon = Some(icon_path.clone());
                        }
                    }
                }

                icon
            };

            if icon.is_none() {
                eprintln!("{:?}: No icon found.", v.0);
            }
            new_db.add_entry(k, EntryData::Command { path: v.0, icon })
        }
    }

    serde_json::to_writer_pretty(File::create_new(path).unwrap(), &new_db).unwrap();
}


fn parse_desktop_file(desktop_file: PathBuf, ini: &mut Ini, apps: &HashMap<String, (PathBuf, Mime)>, scripts: &HashMap<String, (PathBuf, Mime)>,
                      icons: &HashMap<String, PathBuf>) -> Option<EntryData> {

    let map = match ini.load(&desktop_file) {
        Ok(m) => m,
        Err(e) => {
            eprintln!("{desktop_file:?}: Unable to parse ini file. {e}");
            return None;
        },
    };

    match map.get("desktop entry") {
        Some(desktop_entry) => {
            match desktop_entry.get("type") {
                Some(Some(e)) if e == "Application" => {},
                _ => {
                    return None;
                },
            };

            if let Some(Some(e)) = desktop_entry.get("nodisplay") {
                if e == "true" {
                    return None;
                }
            };

            let (executable, args) = match desktop_entry.get("exec") {
                Some(Some(e)) => {
                    let mut splits = e.split(' ');
                    let exec = {
                        let exec = splits.next().expect("No executable found");

                        if !exec.starts_with('/') {
                            if let Some(path) = apps.get(exec) {
                                path.0.clone()
                            } else if let Some(path) = scripts.get(exec) {
                                path.0.clone()
                            } else {
                                eprintln!("{desktop_file:?}: No an app named '{exec}' found.");
                                return None;
                            }
                        } else {
                            let path = PathBuf::from(exec);

                            if !path.exists() || !path.is_executable() {
                                eprintln!("{path:?}: cannot be executed");
                                return None;
                            }

                            path
                        }
                    };

                    // https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html#exec-variables
                    let args: Vec<_> = splits
                        .filter(|s| !(*s == "%f" || *s == "%F" || *s == "%u" || *s == "%U"))
                        .map(&str::to_string).collect();


                    (exec, args)
                },
                _ => {
                    eprintln!("{desktop_file:?}: No Exec key in [Desktop Entry] section");
                    return None;
                },
            };

            let icon = match desktop_entry.get("icon") {
                Some(Some(e)) => {
                    if !e.starts_with('/') {
                        if let Some(path) = icons.get(e) {
                            Some(path.clone())
                        } else if let Some(path) = icons.get(&format!("{e}-symbolic")) {
                            Some(path.clone())
                        } else {
                            eprintln!("{desktop_file:?}: No an icon named '{e}' found.");
                            None
                        }
                    } else {
                        let path = PathBuf::from(e);
                        let ext = path.extension()?;

                        if !path.exists() || !(ext == "png" || ext == "svg") {
                            eprintln!("{path:?}: cannot be used as icon");
                            None
                        } else {
                            Some(path)
                        }
                    }
                },
                _ => {
                    println!("{desktop_file:?}: No Icon key in [Desktop Entry] section");
                    None
                },
            };

            Some(EntryData::Application { path: executable, args, icon })
        },
        None => {
            eprintln!("{desktop_file:?}: No [Desktop Entry] section");
            None
        },
    }
}

