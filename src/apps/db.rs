use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::{thread, time::Duration};
use std::sync::{Arc, atomic::{AtomicPtr, Ordering}};

use ouroboros::self_referencing;

use super::entry_table::SerializableDb;
use super::searcher::{OngoingSearchStateRef, OptionsAccumulator};
use super::EntryData;



struct SearchEntries(HashMap<String, Vec<EntryData>>);

impl SearchEntries {
    fn new() -> Self {
        Self(HashMap::new())
    }

    fn merge_serializable_db(&mut self, db: SerializableDb) {
        for (k, v) in db.0 {
            match self.0.entry(k) {
                Entry::Occupied(mut o) => { o.get_mut().push(v); },
                Entry::Vacant(e) => { e.insert(vec![v]); },
            }
        }
    }
}

#[self_referencing]
struct Database {
    db: SearchEntries,

    #[borrows(db)]
    #[covariant]
    keys: Vec<&'this str>,
}

#[derive(Debug)]
pub struct DatabaseHandle (AtomicPtr<Arc<Database>>);

impl DatabaseHandle {
    pub fn initialize_async() -> Arc<Self> {
        let db = Arc::new(DatabaseHandle (AtomicPtr::new(std::ptr::null_mut())));

        let db_copy = db.clone();
        thread::spawn(move || {

            let mut search_entries = SearchEntries::new();
            {
                let db = SerializableDb::read_system_db();
                search_entries.merge_serializable_db(db);
            }

            let db = DatabaseBuilder {
                db: search_entries,
                keys_builder: |m: &SearchEntries| {
                    let mut vec = Vec::new();

                    for k in m.0.keys() {
                        vec.push(k.as_str());
                    }

                    vec
                },
            }.build();

            db_copy.0.store(Box::leak(Box::new(Arc::new(db))), Ordering::Release);
        });

        db
    }

    pub fn search(&self, text: &str, state: OngoingSearchStateRef, accumulator: &mut OptionsAccumulator) {
        macro_rules! return_if_not_searching {
            () => {
                if !state.is_searching() {
                    return;
                }
            }
        }

        loop {
            let db_ptr = self.0.load(Ordering::Acquire);
            let db = match unsafe { db_ptr.as_ref() } {
                Some(db) => db.clone(),
                None => {
                    thread::sleep(Duration::from_millis(100));
                    return_if_not_searching!();
                    continue;
                },
            };

            return_if_not_searching!();

            let options = rust_fuzzy_search::fuzzy_search_best_n(text, db.borrow_keys(), 6);

            let mut count = 0;
            for (opt, score) in options.into_iter() {
                if (count <= 2 && score >= 0.2) || score >= 0.5 {
                    let entries_data = db.borrow_db().0.get(opt).unwrap();

                    for entry in entries_data {
                        accumulator.add_option(opt.to_string(), entry.clone());
                        count += 1;

                        if count >= 10 { break; }
                    }

                    if count >= 6 { break; }
                }
            }

            return;
        }
    }
}

