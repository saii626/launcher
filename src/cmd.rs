use std::sync::OnceLock;

use clap::Parser;


#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
pub struct CmdOptions {
    /// Reset applications db
    #[arg(long)]
    pub reset_app_db: bool,

    #[arg(long)]
    pub dmenu: bool,

    #[arg(short, long)]
    pub prompt: Option<String>,
}

static COMMAND_LINE_OPTIONS: OnceLock<CmdOptions> = OnceLock::new();

pub fn get_command_line_options() -> &'static CmdOptions {
    COMMAND_LINE_OPTIONS.get().expect("Cmdline options not parsed")
}


pub fn parse_command_line_options() {
    let options = CmdOptions::parse();
    COMMAND_LINE_OPTIONS.set(options).unwrap()
}
