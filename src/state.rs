
use std::sync::{atomic::{AtomicPtr, Ordering}, Arc};
use std::fmt::Debug;

use slint::Timer;


pub struct PossibleOptions<T: Send + Sync + Debug + 'static> {
    options: AtomicPtr<Arc<[T]>>,
}

impl<T: Send + Sync + Debug + 'static> PossibleOptions<T> {
    pub fn new() -> Self {
        Self { options: AtomicPtr::new(std::ptr::null_mut()) }
    }

    pub fn set(&self, options: Vec<T>) {
        let slice_ref: Arc<[T]> = options.into();
        let existing_results = self.options.swap(Box::leak(Box::new(slice_ref)), Ordering::SeqCst);

        if !existing_results.is_null() {
            let existing_results = unsafe { Box::from_raw(existing_results) };
            Self::free_results_when_done(existing_results);
        }
    }

    pub fn get(&self) -> Arc<[T]> {
        match unsafe { self.options.load(Ordering::Acquire).as_ref() } {
            Some(r) => r.clone(),
            None => unreachable!(),
        }
    }

    fn free_results_when_done(results: Box<Arc<[T]>>) {
        // if this is the only reference, allow it to go out of scope, dropping the whole structure
        // i.e. Box and the Arc
        if Arc::strong_count(&results) == 1 {
            //println!("No other refeernces for {:?}. Dropping search options", results);
        } else {
            //println!("There are other refeernces. Delaying the cleanup");

            slint::invoke_from_event_loop(move || {
                Timer::single_shot(std::time::Duration::from_millis(500), move || {
                    Self::free_results_when_done(results);
                });

            }).unwrap();
        }
    }
}


